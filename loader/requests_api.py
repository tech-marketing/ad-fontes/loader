import requests

BASE_URL = 'api.stackexchange.com/2.3'
COLLECTIVE_ENDPOINT_QUESTIONS = 'https://api.stackexchange.com/2.3/collectives/gitlab/questions'

payload = {'page': 1, 'pagesize': 1, 'order': 'desc', 'sort': 'creation', 'site': 'stackoverflow'}
questions = requests.get(COLLECTIVE_ENDPOINT_QUESTIONS, params=payload)

print(questions.url)
print(questions.json()['items'][0]['title'])
print(f"question asked was:\n{questions.json()['items'][0]['title']}")
