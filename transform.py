# this one takes the json writes it to disk with the fields of interest
from collections import defaultdict
from bs4 import BeautifulSoup
import string
import re
import logging

logging.basicConfig(level=logging.INFO, format="%(asctime)-15s %(message)s")
logger = logging.getLogger()

# 1. Read JSON


def transform_json(questions):
    """Read the JSON  given by the LoaderEngine and filters the fields of relevance
    input: questions.json() of type dict
    output: a list containing filtered dictionaries """
    def clean_text(body):
        corpus = BeautifulSoup(body, 'html.parser')
        code_snippets = corpus.find_all('code')
        for code_snippet in code_snippets:
            code_snippet.extract()
        tag_p = re.compile(r'<.*?>')
        corpus = tag_p.sub('', str(corpus)).lower()
        corpus = corpus.translate(str.maketrans('', '', string.punctuation))
        corpus = ' '.join([word for word in corpus.split()])
        return corpus

    filtered_json = []
    data_dict = defaultdict()
    # 2. Extract fields of relevance
    for element in questions['items']:
        try:
            data_dict['creation_date'] = element['creation_date']
            data_dict['user_id'] = element['owner']['user_id']
            data_dict['reputation'] = element['owner']['reputation']
            data_dict['view_count'] = element['view_count']
            data_dict['title'] = element['title']
            data_dict['body'] = clean_text(element['body'])
            filtered_json.append(data_dict.copy())

        except KeyError:
            logger.info(f"WARNING: Key error in parsing -- Data incomplete in: {element}")

    # 3. Return the new filtered JSON
    return filtered_json



