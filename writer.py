"""

takes the filtered out JSON  and writes to DB .

Author: William Arias

v 1.0

"""

from google.cloud.sql import connector
import sqlalchemy
import pg8000
import os


def init_engine():
    def connect_db():
        conn = connector.connect(
            os.environ['INSTANCE_CONNECTION_NAME'],
            'pg800',
            user=os.environ['USER'],
            password=os.environ['PASSWORD'],
            db=os.environ['DB'])
        print("finish")
        return conn

    engine = sqlalchemy.create_engine(
        "postgresql+pg8000://",
        creator=connect_db()

    )
    engine.dialect.description_encoding = None
    return engine


init_engine()