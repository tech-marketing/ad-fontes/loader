import psycopg2


def db_connector(dbname, user, password):
    print(dbname, user)
    try:
        conn = psycopg2.connect(dbname=dbname, user=user, password=password, host="localhost", port=3306)
        conn.set_session(autocommit=True)
        cur = conn.cursor()
        cur.execute("CREATE DATABASE testGL WITH ENCODING 'utf8'")
        #cur.execute("DROP DATABASE test3 ")

        conn.close()

        return cur, conn

    except psycopg2.Error as e:
        print("cursor creation failed")
        print(e)

        return e


