
import os


def create_proxy():
    # beware of loader path, works for local not remote, this function will be deprecated

    command = f"./cloud_sql_proxy -credential_file={os.environ['GCP_SERVICE_ACCOUNT']} -instances={os.environ['INSTANCE_CONNECTION_NAME']}=tcp:3306&"

    
    os.system(command)


